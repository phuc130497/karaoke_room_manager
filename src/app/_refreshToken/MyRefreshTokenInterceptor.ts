import { HttpInterceptor, HttpRequest,
  HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Injectable, Injector } from "@angular/core";
import { Observable } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

@Injectable()
export class MyRefreshTokenInterceptor implements HttpInterceptor {
  constructor(private injector: Injector) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    if (localStorage.getItem('auth-token')) {
      return next.handle(this.modifyRequest(req))

    } else {
      // TODO đăng xuất
      return next.handle(req);
    }
  }

  private modifyRequest(req: any) {
    return req.clone({setHeaders: {'Authorization': 'Bearer '+ localStorage.getItem('auth-token')}});
  }
}
