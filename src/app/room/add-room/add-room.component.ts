import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TokenStorageService } from 'src/app/_service/token-storage.service';
import { RoomServiceService } from '../../_service/room-service.service';

@Component({
  selector: 'app-add-room',
  templateUrl: './add-room.component.html',
  styleUrls: ['./add-room.component.scss']
})
export class AddRoomComponent implements OnInit {

  roomForm!: FormGroup;
  errorFrom: boolean = false;
  constructor(
    private formBuilder: FormBuilder,
    private roomService: RoomServiceService,
    private tokenStorage: TokenStorageService,
    private toastr: ToastrService,
    private router: Router

  ) { }

  ngOnInit() {
    this.createRoomForm();
  }
  createRoomForm() {
    this.roomForm = this.formBuilder.group({
      name: new FormControl('', [Validators.required]),
      price: new FormControl('', [Validators.required]),
      dvt: new FormControl('', [Validators.required]),
      status: new FormControl('', [Validators.required]),
      images: new FormControl('')
    })
  }
  addRoom() {
    if (this.roomForm.valid) {
      var bearerToken = this.tokenStorage.getToken();
      this.roomService.addRoom(bearerToken, this.roomForm.value).subscribe(data => {
        console.log(data);

        if (data.status === 200) {
          this.errorFrom = false;
          this.toastr.success('Thêm Mới Thành Công!', 'Thông báo');
          this.router.navigateByUrl('/room/list-room');
        } else {
          this.errorFrom = false;
          this.toastr.error('Tên Phòng đã Tồn Tại!', 'Thông báo');
        }

      })

    } else {
      this.errorFrom = true;
    }
  }
}
