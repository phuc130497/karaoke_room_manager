import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Room } from 'src/app/_model/room';
import { RoomServiceService } from '../../_service/room-service.service';
import { TokenStorageService } from '../../_service/token-storage.service';
import { HttpHandler } from '@angular/common/http';

declare var $: any;
@Component({

  selector: 'app-list-room',
  templateUrl: './list-room.component.html',
  styleUrls: ['./list-room.component.scss']
})
export class ListRoomComponent implements OnInit {

  listRooms!: Room[];
  Room!: Room;
  roomForm!: FormGroup;

  constructor(

    private roomService: RoomServiceService,
    private router: Router,
    private tokenStorage: TokenStorageService,
    private toastr: ToastrService,
    private formBuilder: FormBuilder,
    private handle: HttpHandler
  ) { }

  ngOnInit() {
    this.findAll();
    this.createRoomForm();
  }

  createRoomForm() {
    this.roomForm = this.formBuilder.group({
      id: [""],
      name: [],
      price: [""],
      dvt: [""],
      status: [""],
      images: [""]
    })
  }

  findAll() {
    var bearerToken = this.tokenStorage.getUser().data.token;
    // debugger
    // console.log(bearerToken);
    return this.roomService.findAllRooms(bearerToken).subscribe(result => {
      this.listRooms = result.data;
      console.log(this.listRooms);

    },
      error => {
        if (error.status === 403) {
          window.localStorage.clear();
        }
        console.log(error);

      }
    )

  }
  deleteRoom(id: number) {

    var bearerToken = this.tokenStorage.getUser().data.token;

    return this.roomService.deleteRoom(bearerToken, id).subscribe(result => {

      this.toastr.success('Xóa Thành Công!', 'Thông báo')

      this.findAll();
    });

  }
  editRoom(id: number) {
    var bearerToken = this.tokenStorage.getUser().data.token;

    return this.roomService.getRoom(bearerToken, id).subscribe(result => {

      this.Room = result.data;
    })

  }
  updateRoom() {
    var bearerToken = this.tokenStorage.getUser().data.token;
    // console.log(this.roomForm.value);
    return this.roomService.updateRoom(bearerToken, this.roomForm.value).subscribe(result => {
      if (result.status === 200) {
        this.toastr.success('Cập nhật Thành Công !', 'Thông báo!');
        this.findAll();
        $("#exampleModalCenter").modal("hide");
      } else {
        $("#exampleModalCenter").modal("hide");
        this.toastr.error('Cập nhật Thất Bại !', 'Thông báo!');
      }

    })


  }
}
