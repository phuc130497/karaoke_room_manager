import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { TokenStorageService } from 'src/app/_service/token-storage.service';


@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.scss']
})
export class RoomComponent implements OnInit {
  detailUser: any;
  constructor(
    private location: Location,
    private toastr: ToastrService,
    private router: Router,
    private tokenStorage: TokenStorageService
  ) { }

  ngOnInit() {
    this.detailAccount();
  }
  detailAccount(){

    this.detailUser = this.tokenStorage.getUser().data;
    console.log(this.detailUser);


  }

  logout() {
    window.localStorage.clear();
    this.router.navigateByUrl('login');
    this.toastr.success('Đăng xuất Thành công', 'Thông báo!');
  }

}
