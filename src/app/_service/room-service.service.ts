import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { baseUrl } from 'src/environments/environment';
import { retry, catchError } from 'rxjs/operators';
import { Room } from '../_model/room';
import { ToastrService } from 'ngx-toastr';



@Injectable({
  providedIn: 'root'
})
export class RoomServiceService {

  constructor(
    private http: HttpClient,
    private toast: ToastrService
  ) { }

  // createAuthorizationHeader(bearerToken: string): HttpHeaders {
  //   const headerDict = {
  //     Authorization: 'Bearer ' + bearerToken,
  //  }
  //   return new HttpHeaders(headerDict);
  // }
  findAllRooms(bearerToken: string): Observable<any>{
    // debugger
    // this.createAuthorizationHeader(bearerToken)
    console.log("1:Room-Service");
    // console.log(this.createAuthorizationHeader(bearerToken));
  //   , {

  //     headers: this.createAuthorizationHeader(bearerToken)

  //  }
    return this.http.get(`${baseUrl}room/all`)
  }

  deleteRoom(bearerToken: string, id: number): Observable<any>{

    return this.http.delete(`${baseUrl}room/` + id)
      .pipe(
      retry(1),
      catchError(this.handleError)
    );



  }
  getRoom(bearerToken: string, id: number): Observable<any> {

    return this.http.get(`${baseUrl}room/` + id)

  }
  updateRoom(bearerToken: string, room: Room): Observable<any>{
    return this.http.put(`${baseUrl}room`, room)
  }
  addRoom(bearerToken: string, room: any):Observable<any> {
    return this.http.post<Room[]>(`${baseUrl}room`, room)
  }

  handleError(error: any) {
    debugger
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = 'Sự cố máy không có mạng !';
    } else {
      // server-side error
      errorMessage = 'Chúng tôi đang gặp sự cố vui lòng đợi trong giây lát';

    }
    // window.alert("Lỗi!")
    // this.toast.error('Chúng tôi đang gặp sự cố vui lòng đợi trong giây lát', 'Thông báo!');
    return throwError(errorMessage);
  }

}
