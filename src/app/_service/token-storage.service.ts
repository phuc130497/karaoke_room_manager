import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

const TOKEN_KEY = 'auth-token';
const USER_KEY = 'auth-user';

@Injectable({
  providedIn: 'root'
})
export class TokenStorageService {

  // constructor(private toastr: ToastrService) { }
  constructor() { }

signOut(): void {
  // this.sendSuccess()
  window.localStorage.clear();
  document.location.reload();
}
// sendSuccess() {
//   this.toastr.success('Đăng xuất thành công', 'Thông báo');
// }
public saveToken(token: string): void {
  window.localStorage.removeItem(TOKEN_KEY);
  window.localStorage.setItem(TOKEN_KEY, token);
}

public getToken(): string {
  return window.localStorage.getItem(TOKEN_KEY)!;
}

public saveUser(user:any): void {
  window.localStorage.removeItem(USER_KEY);
  window.localStorage.setItem(USER_KEY, JSON.stringify(user));
}

public getUser(): any {
  return JSON.parse(localStorage.getItem(USER_KEY) || '{}');
}
}
