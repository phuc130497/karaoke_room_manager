export interface Room {
  id: number;
  name: string;
  price: number;
  dvt: string;
  status: string;
  images: string;

}
