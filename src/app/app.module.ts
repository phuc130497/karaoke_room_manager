import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AuthGuardService } from './_service/auth-guard.service';
import { ListRoomComponent } from './room/list-room/list-room.component';
import { AddRoomComponent } from './room/add-room/add-room.component';
import { EditRoomComponent } from './room/edit-room/edit-room.component';
import { RoomComponent } from './room/room/room.component'

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { MyRefreshTokenInterceptor } from './_refreshToken/MyRefreshTokenInterceptor';
// import { AngularFontAwesomeModule } from 'angular-font-awesome';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ListRoomComponent,
    AddRoomComponent,
    EditRoomComponent,
    RoomComponent

   ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    FontAwesomeModule,

    // AngularFontAwesomeModule,
    ToastrModule.forRoot({
      timeOut: 6000,
      positionClass: 'toast-bottom-right'
    })
  ],
  providers: [AuthGuardService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: MyRefreshTokenInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
