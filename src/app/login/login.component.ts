import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { LoginServiceService } from '../_service/login-service.service';
import { TokenStorageService } from '../_service/token-storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  formGroup!: FormGroup;
  isLogin: boolean = false;
  isLoggedIn : boolean = false;
  isLoginError : boolean = false;
  constructor(
    private loginService: LoginServiceService,
    private tokenStorage: TokenStorageService,
    private router: Router,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.initForm();
  }
  initForm() {
    this.formGroup = new FormGroup({
      username: new FormControl('', [Validators.required]),
      password: new FormControl('',[Validators.required])
    });
  }
  loginProces() {
    if (this.formGroup.valid) {

      this.isLogin = false;

      this.loginService.login(this.formGroup.value).subscribe(

        result => {
          console.log(result);

          if (result.status == 200) {

            // login Thanh Cong
            this.tokenStorage.saveToken(result.data.token);
            this.tokenStorage.saveUser(result);
            this.isLoggedIn = true;
            this.router.navigateByUrl('room');

            // alert(result.message);
            // this.isLogin = false;
            this.showSuccess();

          } else {
            this.showError();
            this.isLogin = false;
            // this.isLoginError = true;
          }

      })
    } else {
      this.isLogin = true;
    }
    this.showError();
  }
  showSuccess() {
    this.toastr.success( 'Đăng nhập thành công','Thông báo!');
  }
  showError() {
    this.toastr.error( 'Đăng nhập thất bại','Thông báo!');
  }

}
