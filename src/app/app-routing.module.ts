import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { ListRoomComponent } from './room/list-room/list-room.component';
// import { RoomComponent } from './room/room.component';
import { AuthGuardService } from './_service/auth-guard.service';
import { AddRoomComponent } from './room/add-room/add-room.component';
import { EditRoomComponent } from './room/edit-room/edit-room.component';
import { RoomComponent } from './room/room/room.component';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  {
    path: 'room', canActivate: [AuthGuardService],component: RoomComponent,
    children: [
      { path: '', redirectTo: 'list-room', pathMatch: 'full' },
      { path: 'list-room', component: ListRoomComponent },
      { path: 'add-room', component: AddRoomComponent },
      { path: 'edit-room', component: EditRoomComponent }
      ]
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
